'''
sha256 cryptography algorithm
'''
from cryptography.hazmat.primitives import hashes


def sha256(data: bytes) -> bytes:
    '''
    main function
    '''
    digest = hashes.Hash(hashes.SHA256())
    digest.update(data)
    return digest.finalize()
