'''
test algorithm
'''
from sha256 import sha256
from sm3 import sm3

if __name__ == '__main__':
    STRING = b'hello BUAA cryptography'

    print('sha256', sha256(STRING))
    print('sm3:', sm3(STRING))
