'''
sm3 cryptography algorithm
'''
from cryptography.hazmat.primitives import hashes


def sm3(data: bytes) -> bytes:
    '''
    main function
    '''
    digest = hashes.Hash(hashes.SM3())
    digest.update(data)
    return digest.finalize()
